import Vuex from 'vuex'
import auth from './modules/auth'

export default Vuex.createStore({
  state: {
    user: [
      { email: 'student01@gmail.com', password: 'abc123', name: 'Areeya Dengjaroen', id: '62160319', role: 'Student' },
      { email: 'student02@gmail.com', password: 'abc123', name: 'Pattharawan Rassami', id: '62160292', role: 'Student' },
      { email: 'teacher03@gmail.com', password: 'abc123', name: 'Jenny Kim', id: '000001', role: 'Teacher' },
      { email: 'teacher04@gmail.com', password: 'abc123', name: 'James Tom', id: '000002', role: 'Teacher' }
    ]
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth
  }
})
