import { AUTH_LOGIN, AUTH_LOGOUT } from './mutation-type'
import router from '../../router'
export default {
  namespaced: true,
  state: () => ({
    user: null
  }),
  mutations: {
    [AUTH_LOGIN] (state, payload) {
      state.user = payload
    },
    [AUTH_LOGOUT] (state) {
      state.user = null
    }
  },
  actions: {
    login ({ commit }, payload) {
      const user = { name: 'Administrator', email: 'admin@user.com' }
      localStorage.setItem('user', user)
      commit(AUTH_LOGIN, user)
      router.push('/Home')
    },
    logout ({ commit }) {
      commit(AUTH_LOGOUT)
      router.push('/')
    }
  },
  getters: {
    isLogin (state, getters) {
      return state.user != null
    }
  }
}
