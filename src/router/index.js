import { createRouter, createWebHistory } from 'vue-router'
import Login from '../views/Login.vue'
import Home from '../views/Home.vue'
import Classes from '../views/Classes.vue'
import ClassesStudent from '../views/ClassesStudent'
import NewAcc from '../views/NewAcc.vue'
import RegisterClass from '../views/RegisterClass.vue'
import User from '../views/User.vue'
import Room from '../views/Room.vue'

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
    meta: { title: 'Online system Teacher and Student' }
  },
  {
    path: '/Home',
    name: 'Home',
    component: Home
  },
  {
    path: '/Classes',
    name: 'Classes',
    component: Classes
  },
  {
    path: '/ClassesStudent',
    name: 'ClassesStudent',
    component: ClassesStudent
  },
  {
    path: '/RegisterClass',
    name: 'RegisterClass',
    component: RegisterClass
  },
  {
    path: '/NewAccount',
    name: 'NewAccount',
    component: NewAcc
  },
  {
    path: '/User',
    name: 'User',
    component: User
  },
  {
    path: '/Room',
    name: 'Room',
    component: Room
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
